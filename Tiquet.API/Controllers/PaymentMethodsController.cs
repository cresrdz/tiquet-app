﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tiquet.Repository.Catalogs;
using Microsoft.EntityFrameworkCore;

namespace Tiquet.API.Controllers
{
    [Produces("application/json")]
    [Route("api/PaymentMethods")]
    public class PaymentMethodsController : Controller
    {
        private PaymentMethodRepository _repository = new PaymentMethodRepository();

        [HttpGet("/paymentmethods")]
        public async Task<IActionResult> List()
        {
            var paymentMethod = await _repository.List().ToListAsync();
            return new OkObjectResult(paymentMethod);
        }
    }
}