﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tiquet.Data;
using Tiquet.Data.Models;
using Tiquet.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Tiquet.Repository.Catalogs
{
    public class PaymentMethodRepository : IBaseRepository<PaymentMethod>
    {
        private readonly TiquetContext _context = new TiquetContext();

        public PaymentMethodRepository() { }

        public void Destroy()
        {
            throw new NotImplementedException();
        }

        public async Task<PaymentMethod> Get()
        {
            throw new NotImplementedException();
        }

        public IQueryable<PaymentMethod> List()
        {
            return _context.PaymentMethod.AsQueryable();
        }

        public PaymentMethod Save()
        {
            throw new NotImplementedException();
        }

        public PaymentMethod Update()
        {
            throw new NotImplementedException();
        }
    }
}
