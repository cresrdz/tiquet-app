﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tiquet.Repository.Interfaces
{
    public interface IBaseRepository<T>
    {
        T Save();
        T Update();
        Task<T> Get();
        IQueryable<T> List();
        void Destroy();
    }
}
