﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tiquet.Data.Interfaces
{
    public interface IBaseModel
    {
        Guid Id { get; set; }
        string Name { get; set; }
        string Description { get; set; }
    }
}
