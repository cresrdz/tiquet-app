﻿using System;
using System.Collections.Generic;
using System.Text;
using Tiquet.Data.Interfaces;

namespace Tiquet.Data.Models
{
    public class PaymentMethod : IBaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public PaymentMethod() { }
    }
}
